
resource "aws_vpc" "ninja-vpc-01" {
  cidr_block = var.vpcCidr
  enable_dns_hostnames = true
  enable_dns_support = true
  tags = {
    Name = var.vpcName
  }
}

resource "aws_subnet" "public" {
  count      = length(var.pub_sub_cidrs)
  vpc_id     = aws_vpc.ninja-vpc-01.id
  cidr_block = var.pub_sub_cidrs[count.index]
  map_public_ip_on_launch = true
  tags = {
    Name = var.pub_sub_name[count.index]
  }

}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.ninja-vpc-01.id

  tags = {
    Name = var.igw_name
  }
}


resource "aws_route_table" "pub-route" {
  vpc_id = aws_vpc.ninja-vpc-01.id

  route {
    cidr_block = var.routetb_cidr
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
    Name = var.pub_routetb
  }
}


resource "aws_route_table_association" "public" {
  count          = length(var.pub_sub_cidrs)
  subnet_id      = aws_subnet.public[count.index].id
  route_table_id = aws_route_table.pub-route.id
}

  

resource "aws_security_group" "wg" {
  name        = "wg"
  vpc_id = aws_vpc.ninja-vpc-01.id
  description = "Terraform Managed. Allow Wireguard client traffic from internet."
  tags = {
    Name       = "wireguard"
  }

  ingress {
    from_port   = 0
    to_port     = 65535
    protocol    = "udp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

    ingress {
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]

  }
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}


resource "aws_instance" "wg" {
  count      = length(var.instance_name)
  subnet_id      = aws_subnet.public[count.index].id
  ami           = data.aws_ami.ubuntu.id
  instance_type = var.instance_type
  security_groups = [aws_security_group.wg.id]
  key_name = "allServers"
  tags = {
    Name = var.instance_name[count.index]
  }
}

