vpcCidr = "192.168.0.0/24"

vpcName = "wg-vpc"

pub_sub_cidrs = ["192.168.0.0/26", "192.168.0.64/26"]

priv_sub_cidrs = ["192.168.0.128/26", "192.168.0.192/26"]

pub_sub_name = ["wg-pub-sub-01", "wg-pub-sub-02"]

priv_sub_name = ["wg-priv-sub-01", "wg-priv-sub-02"]

igw_name = "igw-01-wg"

nat_name = "wg-nat-01-wg"

priv_routetb = "wg-route-priv-01/02"

pub_routetb = "wg-route-pub-01/02"

routetb_cidr = "0.0.0.0/0"