variable "vpcCidr" {
  default = "10.0.0.0/16"
}

variable "vpcName" {
  default = "ninja-vpc-01"
}

variable "pub_sub_cidrs" {
  default = ["10.0.0.0/24", "10.0.1.0/24"]
}

variable "priv_sub_cidrs" {
  default = ["10.0.2.0/24", "10.0.3.0/24"]
}



variable "pub_sub_name" {
  default = ["ninja-pub-sub-01", "ninja-pub-sub-02"]
}



variable "priv_sub_name" {
  default = ["ninja-priv-sub-01", "ninja-priv-sub-02"]
}


variable "igw_name" {
  default = "igw-01"
}

variable "nat_name" {
  default = "ninja-nat-01"
}

variable "priv_routetb" {
  default = "ninja-route-priv-01/02"
}

variable "pub_routetb" {
  default = "ninja-route-pub-01/02"
}

variable "routetb_cidr" {
  default = "0.0.0.0/0"
}

variable "instance_name" {
  default = ["wg-bastion-1", "wg-bastion-2"]
}

variable "instance_type" {
  default = "t2.micro"
}